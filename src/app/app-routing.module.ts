import { InscriptionComponent } from './pages/inscription/inscription.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfirmationComponent } from './pages/confirmation/confirmation.component';
import { ScannerComponent } from './pages/scanner/scanner.component';

const routes: Routes = [
  {path: 'inscription', component: InscriptionComponent},
  {path: 'confirmation', component: ConfirmationComponent},
  {path: 'scanner', component: ScannerComponent},
  {path: '**', redirectTo: 'inscription'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
