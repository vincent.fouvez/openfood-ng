import { User } from '../models/Users.model';
import { Injectable } from '@angular/core';
@Injectable({providedIn: 'root'})
export class InscriptionService {
  private curentUsers: User ;

  addUser(user: User) {
    this.curentUsers = user;

  }
  getCurentUser(): User {
    return this.curentUsers;
  }

}
