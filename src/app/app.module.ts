import { InscriptionService } from './services/inscription.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { InscriptionComponent } from './pages/inscription/inscription.component';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { ConfirmationComponent } from './pages/confirmation/confirmation.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {LayoutModule} from '@angular/cdk/layout';
import {HttpClientModule} from '@angular/common/http';
import { ScannerComponent } from './pages/scanner/scanner.component';
import { BarecodeScannerLivestreamModule } from 'ngx-barcode-scanner';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    InscriptionComponent,
    ConfirmationComponent,
    ScannerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatButtonToggleModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    HttpClientModule,
    BarecodeScannerLivestreamModule
  ],
  providers: [InscriptionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
