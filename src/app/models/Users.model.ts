export class User {
  constructor(
    private nom: string,
    private prenom: string,
    private pseudo: string,
    private email: string
  ) {}
}
