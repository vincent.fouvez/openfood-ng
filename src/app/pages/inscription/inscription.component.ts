import { User } from './../../models/Users.model';
import { InscriptionService } from './../../services/inscription.service';
import { Component, OnInit, Output } from '@angular/core';
import {
  Validators,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {
  inscriptionForm: FormGroup;

  constructor(
    private inscriptionService: InscriptionService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.inscriptionForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      pseudo: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }
  onSubmitForm() {
    const formValue = this.inscriptionForm.value;
    const newUser = new User(
      formValue.nom,
      formValue.prenom,
      formValue.pseudo,
      formValue.email
    );

    this.inscriptionService.addUser(newUser);
    this.router.navigate(['/confirmation']);
  }
}
