import { User } from './../../models/Users.model';
import { InscriptionService } from './../../services/inscription.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
  users: User ;


  constructor(private inscriptionService: InscriptionService,
              ) {}

  ngOnInit() {
    this.users = this.inscriptionService.getCurentUser();

  }
}
